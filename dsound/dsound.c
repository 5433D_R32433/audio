#include "dsound.h"
#include "wave.h"

#define DR_WAV_IMPLEMENTATION
#include "dr_wav.h"

#include <time.h>
#include <windows.h>

#define CHECK_LAST_ERROR()                                         \
if ( GetLastError ( ) )                                            \
{                                                                  \
	wchar_t buffer  [ 512 ] = { 0 };                               \
	wchar_t caption [ 128 ] = { 0 };                               \
	FormatMessageW ( FORMAT_MESSAGE_FROM_SYSTEM |                  \
					 FORMAT_MESSAGE_IGNORE_INSERTS,                \
					 0,                                            \
					 GetLastError ( ),                             \
					 MAKELANGID ( LANG_NEUTRAL, SUBLANG_DEFAULT ), \
					 buffer,                                       \
					 ARRAY_COUNT ( buffer ),                       \
					 0 );                                          \
	_snwprintf  ( caption, 128, L"0x%X", GetLastError ( )  );      \
	MessageBoxW ( 0, buffer, caption, MB_OK | MB_ICONERROR );      \
}


static LRESULT CALLBACK dummy_wnd_proc ( HWND hwnd, UINT msg, WPARAM wp, LPARAM lp )
{
	switch ( msg )
	{
		case WM_CREATE:
		{
		} break;
		case WM_DESTROY:
		{
			DestroyWindow ( hwnd );
			PostMessage ( hwnd, WM_CLOSE, 0, 0 );
			PostQuitMessage ( 0 );
		} break;
		default:
		{
			return DefWindowProc ( hwnd, msg, wp, lp );
		} break;
	}
	return 0;
}

HWND create_dummy_window ( )
{
	WNDCLASSEXW  wcx    = { 0 };
	wcx.cbSize          = sizeof ( WNDCLASSEXW );
	wcx.hbrBackground   = 0;
	wcx.style           = CS_HREDRAW | CS_VREDRAW;
	wcx.hCursor         = LoadCursor ( NULL, IDC_ARROW );
	wcx.hInstance       = GetModuleHandle ( 0 );
	wcx.lpszClassName   = ( LPCWSTR ) "DUMMY_WINDOW";
	wcx.lpfnWndProc     = ( WNDPROC ) dummy_wnd_proc;	
	RegisterClassExW ( &wcx );
	CHECK_LAST_ERROR ( );
	HWND hwnd = CreateWindowExW ( 0,
		                          wcx.lpszClassName,
		                          L"", 
		                          0,
		                          0, 
								  0, 
								  0, 
								  0, 
								  0, 
								  0, 
								  0, 
								  0 );
	
	ShowWindow ( hwnd, false );
	return hwnd;
}

int main ( int argc, char **argv )
{
	dsound_t dsound  = { 0 };
	dsound_t capture = { 0 };
	u32 channels;
    u32 frequency;
    u64 frame_count;
    i16* data = drwav_open_file_and_read_pcm_frames_s16 ( "Donya.wav", &channels, &frequency, &frame_count, 0 );  
	
	u32 size = ( frame_count * channels * 2 );
	clock_t start, end;
	double t = 0;
	u8* p = 0;
	if ( data ) 
	{
		p = ( u8* ) data;
		u8* end_of_data = ( u8* ) ( ( u8* ) data + size );
		
		dsound  = dsound_open_device  ( create_dummy_window ( ), frequency, channels, 16, true, false );
		

		start = clock ( );
		while ( true )
		{	
			dsound_wait_device ( &dsound );	
			dsound_play_device ( &dsound );
			
			if ( p == ( ( u8* ) data + size ) )
			{
				p = ( u8* ) data;
				start = clock ( );
				t = 0;
			}
			u8* b = dsound_get_device_buffer ( &dsound );
			assert ( b );	
			if ( b )
			{
				u32 how_much = ( end_of_data - p ) < dsound.size ? ( size % dsound.size ) : dsound.size;
				if ( how_much < dsound.size )
				{
					u8* tmp = ( u8* ) malloc ( dsound.size );
					assert ( tmp );
					memset ( tmp, 0, dsound.size );
					memcpy ( tmp, p, how_much );
					memcpy ( b, tmp, dsound.size );
					p = ( u8* ) data;
					t = 0;
					start = clock ( );
					free ( tmp );
				}
				else
				{
					memcpy ( b, p, how_much );
				}
			}
			p += dsound.size;		
			end = clock ( );	
			t = ( ( double ) ( end - start ) / CLOCKS_PER_SEC );
			f64 secs = ( f64 ) ( size / ( frequency * channels * 2 ) );
			printf ( "\rtime: %.01f of %.01f (%02d:%.01f)", t, secs, ( u32 ) secs / 60, ( f64 ) ( ( u32 ) secs % 60 ) );
			fflush ( stdout );
		}	
    }
	
	dsound_close_device ( &dsound );
	drwav_free ( data, 0 );
	return 0;
}


#if 0
	for ( u32 i = 0; i < 127; i++ )
	{
		MIDI_TABLE [ i ] = 440.0f * powf ( 2, ( ( f32 ) i - 69.0f ) / 12.0f );
	}
	for ( u32 i = 0; i < 256; i++ )
	{
		SINE_TABLE [ i ] = ( i16 ) ( 32767.0f * sinf ( TWO_PI * ( ( f32 ) i / 256.0f ) ) );
	}
	for ( u32 i = 0; i < 256; i++ )
	{
		COSINE_TABLE [ i ] = ( i16 ) ( 32767.0f * cosf ( TWO_PI * ( ( f32 ) i / 256.0f ) ) );
	}
	printf ( "Simple synthesizer\n\nSharps: S D G H J K\nWhole notes: Z X C V B N M\n\nQ to quit\n\n" );
	b32 done = false;
	while ( !done )
	{
		switch ( getch ( ) )
		{
			case 'q':
			{
				done = true;
			} break;

			case 'z':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 60 ] ); // C
				printf ( "C %3.3fHz\n", MIDI_TABLE [ 60 ] );
			} break; 

			case 's':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 61 ] ); // C#
				printf           ( "C# %3.3fHz\n", MIDI_TABLE [ 61 ] );
			} break;
			
			case 'x':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 62 ] ); // D
				printf           ( "D %3.3fHz\n", MIDI_TABLE [ 62 ] );
			} break;
			
			case 'd':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 63 ] ); // D#
				printf           ( "D# %3.3fHz\n", MIDI_TABLE [ 63 ] );
			} break;
			
			case 'c':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 64 ] ); // E
				printf           ( "E %3.3fHz\n", MIDI_TABLE [ 64 ] );
			} break;
			
			case 'v':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 65 ] ); // F
				printf           ( "F %3.3fHz\n", MIDI_TABLE [ 65 ] );
			} break;
			
			case 'g':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 66 ] ); // F#
				printf           ( "F# %3.3fHz\n", MIDI_TABLE [ 66 ] );
			} break;
			
			case 'b':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 67 ] ); // G
				printf           ( "G %3.3fHz\n", MIDI_TABLE [ 67 ] );
			} break;
			
			case 'h':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 68 ] ); // G#
				printf           ( "G# %3.3fHz\n", MIDI_TABLE  [ 68 ] );
			} break;
			
			case 'n':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 69 ] ); // A
				printf           ( "A %3.3fHz\n", MIDI_TABLE [ 69 ] );
			} break;
			
			case 'j':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 70 ] ); // A#
				printf           ( "A# %3.3fHz\n", MIDI_TABLE  [ 70 ] );
			} break;
			
			case 'm':
			{
				dsound_play_note ( &ds, MIDI_TABLE [ 71 ] ); // B
				printf           ( "B %3.3fHz\n", MIDI_TABLE [ 71 ] );
			} break;
			
			case 'k':
			{
				dsound_play_note ( &ds, MIDI_TABLE  [ 72 ] ); // B#
				printf           ( "B# %3.3fHz\n", MIDI_TABLE [ 72 ] );
			} break;
			
			default:
			{
				
			} break;
		}
	}
	
#endif