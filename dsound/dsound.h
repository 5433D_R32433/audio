#ifndef __DIRECTSOUND_H__
#define __DIRECTSOUND_H__

#ifdef __cplusplus
extern "C" {
#endif // !__cplusplus

#if defined(_MSC_VER) && (defined(_M_X64) || defined(_M_AMD64) || defined(_M_IX86))
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif

#if !defined(__5433D_HELPERS__)
#define __5433D_HELPERS__

#ifdef _WIN32
#include <Windows.h>
#define DBGPRINTF( ... ) { char buffer [ 512 ] = { 0 }; snprintf ( buffer, sizeof ( buffer ), __VA_ARGS__ ); OutputDebugString ( buffer ); }
#else
#define DBGPRINTF( ... ) { fprintf ( stderr, __VA_ARGS__ ); }
#endif

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET( x ) ( ( u8* ) 0 + ( x ) )
#endif


#define LITTLE_ENDIAN_BYTE_ORDER 0x41424344UL 
#define BIG_ENDIAN_BYTE_ORDER    0x44434241UL
#define PDP_ENDIAN_BYTE_ORDER    0x42414443UL
#define BYTE_ORDER               ( ( 'A' << 24 ) | ( 'B' << 16 ) | ( 'C' << 8 ) | 'D' )

#if BYTE_ORDER==LITTLE_ENDIAN_BYTE_ORDER
#define LITTLE_ENDIAN
#elif BYTE_ORDER==BIG_ENDIAN_BYTE_ORDER
#define BIG_ENDIAN
#elif BYTE_ORDER==PDP_ENDIAN_BYTE_ORDER
#define PDP_ENDIAN
#else
#error "Unknown Bytes Order Endianness"
#endif


#define likely(p)   (!!(p))
#define unlikely(p) (!!(p))


#if defined(_WIN32) && !defined(_WIN64)
typedef long long          i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#elif defined(_WIN64)
typedef __int64            i64;
typedef __int32            i32;
typedef __int16            i16;
typedef __int8              i8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef long int           i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#endif


#ifdef _WIN32
typedef signed long long   s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#elif defined(_WIN64)
typedef signed __int64     s64;
typedef signed __int32     s32;
typedef signed __int16     s16;
typedef signed __int8       s8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef signed long int    s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#endif


#if defined(_WIN32)
typedef unsigned long long   u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#elif defined(_WIN64)
typedef unsigned __int64     u64;
typedef unsigned __int32     u32;
typedef unsigned __int16     u16;
typedef unsigned __int8       u8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef unsigned long int    u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#endif

#if defined(_WIN64)
#define usizemax 0xffffffffffffffffui64
typedef i64 imax;
typedef u64 umax;
typedef u64 usize;
typedef i64 isize;
typedef i64 ptrdiff;
typedef i64 iptr;
typedef u64 uptr;
#elif defined(_WIN32) && !defined(_WIN64)
#define usizemax 0xffffffffui32
typedef i32      imax;
typedef u32      umax;
typedef u32      usize;
typedef i32      isize;
typedef i32      ptrdiff;
typedef i32      iptr;
typedef u32      uptr;
#elif defined(unix) || defined(UNIX) || defined(__unix__) || defined(__UNIX__) || defined(__BSD__) || defined(BSD) || defined(__FreeBSD__) || defined(__NetBSD__) || defined (__OpenBSD__) || defined(__LINUX__)

#elif defined(__APPLE__) && defined(__MACH__)

#endif 

typedef float       f32; 
typedef long double f64;    

typedef i32 b32;
typedef i16 b16;
typedef i8  b8;

#define i8min        (-127i8 - 1)
#define i16min       (-32767i16 - 1)
#define i32min       (-2147483647i32 - 1)
#define i64min       (-9223372036854775807i64 - 1)
#define i8max        127i8
#define i16max       32767i16
#define i32max       2147483647i32
#define i64max       9223372036854775807i64
#define u8max        0xffui8
#define u16max       0xffffui16
#define u32max       0xffffffffui32
#define u64max       0xffffffffffffffffui64


#if !defined(__cplusplus)
#define true  1
#define false 0
#endif

#if defined(__GNUC__) || defined(__clang__)
#define __FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define __FORCEINLINE __forceinline
#else
#define __FORCEINLINE inline
#endif

#ifdef _MSC_VER
# define __PACKED(d) __pragma(pack(push, 1)) d __pragma(pack(pop))
# define __PACKED_START __pragma(pack(push, 1))
# define __PACKED_END   __pragma(pack(pop))
#elif defined (__GNUC__) || defined (__clang__)
# define __PACKED(d) d __attribute__((packed, aligned(1)))
# define __PACKED_START _Pragma("pack(push, 1)")
# define __PACKED_END   _Pragma("pack(pop)")
#endif


#define FREENULL(p) do { free( p ); p = 0; } while(0)
#define EMPTY_STR(str) (!str || !*str)

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b))
#endif // !MIN

#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b))
#endif // !MAX

#define UNUSED(v)        ( ( void ) v )
#define ARRAY_COUNT( a ) ( sizeof ( a ) / sizeof ( ( a ) [ 0 ] ) )

#define ALIGN_DOWN(x,a)      ( ( x ) & ~( ( a ) - 1 ) )
#define ALIGN_UP(x,a)        ALIGN_DOWN ( ( x ) + ( a ) - 1, ( a ) )
#define ALIGN_DOWN_PTR(p, a) ((void *)ALIGN_DOWN((uptr)(p), (a)))
#define ALIGN_UP_PTR(p, a)   ((void *)ALIGN_UP((uptr)(p), (a)))

	
#if defined(_MSC_VER)
__FORCEINLINE u16 BYTESWAP16 ( u16 x ) { return _byteswap_ushort ( x ); }
__FORCEINLINE u32 BYTESWAP32 ( u32 x ) { return _byteswap_ulong  ( x ); }
__FORCEINLINE u64 BYTESWAP64 ( u64 x ) { return _byteswap_uint64 ( x ); }
#elif defined (__GNUC__) || defined (__clang__)
__FORCEINLINE u16 BYTESWAP16 ( u16 x ) { return __builtin_bswap16 ( x ); }
__FORCEINLINE u32 BYTESWAP32 ( u32 x ) { return __builtin_bswap32 ( x ); }
__FORCEINLINE u64 BYTESWAP64 ( u64 x ) { return __builtin_bswap64 ( x ); }
#else
#define BYTESWAP16(x) ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define BYTESWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )  

#define BYTESWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )	
#endif

#define KB(x) (      x   * 1024 )
#define MB(x) ( KB ( x ) * 1024 )
#define GB(x) ( MB ( x ) * 1024 )
#define TB(x) ( GB ( x ) * 1024 )
#define PB(x) ( TB ( x ) * 1024 )

#else
	
#endif // !__5433D_HELPERS__

#include <windows.h>
#include <dsound.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "user32.lib")


#ifndef WAVE_FORMAT_IEEE_FLOAT
#define WAVE_FORMAT_IEEE_FLOAT 0x0003
#endif

#define CHECK_HRESULT(hr)                                                                                          \
	if ( FAILED ( hr ) )                                                                                           \
	{                                                                                                              \
	    char text    [ 2048  ] = { 0 };                                                                            \
	    char caption [ 2048  ] = { 0 };                                                                            \
        int  count  = 0;                                                                                           \
        snprintf ( text, 2048, "File: %s\nFuncion: %s\nLine: %d\n", __FILE__, __FUNCTION__, __LINE__ );            \
        count += snprintf ( caption, 2048, "0x%X: ", hr );                                                         \
		FormatMessage ( FORMAT_MESSAGE_FROM_SYSTEM |                                                               \
                        FORMAT_MESSAGE_IGNORE_INSERTS,                                                             \
                        0,                                                                                         \
                        hr,                                                                                        \
                        MAKELANGID ( LANG_NEUTRAL, SUBLANG_DEFAULT ),                                              \
                        ( LPTSTR ) caption + count,                                                                \
                        sizeof ( text ) - count,                                                                   \
                        0 );                                                                                       \
        MessageBox ( 0, text, caption, MB_OK | MB_ICONERROR );                                                     \
        __debugbreak ( );                                                                                          \
	}



#define SAFE_RELEASE(p)                     \
	{                                       \
		if ( p )                            \
		{						            \
			( p )->lpVtbl->Release ( ( p ) ); \
			( p ) = 0;                      \
		}                                   \
	}
	
#if UNICODE
#define STRINGTOUTF8(S) 
#define UTF8TOSTRING(S)
#else
#define STRINGTOUTF8(S) 
#define UTF8TOSTRING(S)
#endif
	
	
#define SAMPLERATE 44100
f32 MIDI_TABLE [ 127 ] = { 0 };
i16 SINE_TABLE [ 256 ] = { 0 };
i16 COSINE_TABLE [ 256 ] = { 0 };
u16 phase = 0 ;
const f32 TWO_PI = 2.0f * 3.1415926f;



typedef struct dsound_t
{	
	DSCBUFFERDESC			   dscbd;
	LPDIRECTSOUNDCAPTURE       capture;
	LPDIRECTSOUNDCAPTUREBUFFER capture_buffer;
	WAVEFORMATEX               wfx_capture;
	
	LPDIRECTSOUND8             playback;
	DSBUFFERDESC 			   primary_dsbd;
	DSBUFFERDESC 			   secondary_dsbd;
	LPDIRECTSOUNDBUFFER        primary_buffer;
	LPDIRECTSOUNDBUFFER        secondary_buffer;
	WAVEFORMATEX               wfx_playback;
	
	u32                        number_of_buffers;
    u32                        size;
    u32                        last_chunk;
    u8                         *locked_audio_buffer;
} dsound_t;


static void* DSOUND_DLL = NULL;
typedef HRESULT ( WINAPI *_DirectSoundCreate8           )( LPGUID, LPDIRECTSOUND8*, LPUNKNOWN );
typedef HRESULT ( WINAPI *_DirectSoundEnumerateW        )( LPDSENUMCALLBACKW,     LPVOID    );
typedef HRESULT ( WINAPI *_DirectSoundCaptureCreate8    )( LPCGUID,LPDIRECTSOUNDCAPTURE8*,  LPUNKNOWN );
typedef HRESULT ( WINAPI *_DirectSoundCaptureEnumerateW )( LPDSENUMCALLBACKW,               LPVOID    );
static _DirectSoundCreate8           pDirectSoundCreate8           = 0;
static _DirectSoundCaptureCreate8    pDirectSoundCaptureCreate8    = 0;
static _DirectSoundEnumerateW        pDirectSoundEnumerateW        = 0;
static _DirectSoundCaptureEnumerateW pDirectSoundCaptureEnumerateW = 0;

#define DSOUND_LOAD(f) p##f = ( _##f ) ( GetProcAddress ( LoadLibraryA ( "dsound.dll" ), #f ) )

#define DSOUND_UNLOAD(dll)             \
{                                      \
	pDirectSoundCreate8           = 0; \
	pDirectSoundEnumerateW        = 0; \
	pDirectSoundCaptureCreate8    = 0; \
	pDirectSoundCaptureEnumerateW = 0; \
	if ( dll )                         \
	{                                  \
		FreeLibrary ( dll );           \
		dll = 0;                       \
	}                                  \
}


#if 0
/*
WASAPI doesn't need this. This is just for DirectSound/WinMM.
*/
char *
dsound_lookup_audio_device_name ( const wchar_t *name, const GUID *guid )
{
#if __WINRT__
    return WIN_StringToUTF8(name);  /* No registry access on WinRT/UWP, go with what we've got. */
#else
    static const GUID nullguid = { 0 };
    const u8 *ptr;
    char keystr[128];
    WCHAR *strw = NULL;
    SDL_bool rc;
    HKEY hkey;
    DWORD len = 0;
    char *retval = NULL;

    if (WIN_IsEqualGUID(guid, &nullguid)) {
        return WIN_StringToUTF8(name);  /* No GUID, go with what we've got. */
    }

    ptr = (const unsigned char *) guid;
    SDL_snprintf(keystr, sizeof (keystr),
        "System\\CurrentControlSet\\Control\\MediaCategories\\{%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X}",
        ptr[3], ptr[2], ptr[1], ptr[0], ptr[5], ptr[4], ptr[7], ptr[6],
        ptr[8], ptr[9], ptr[10], ptr[11], ptr[12], ptr[13], ptr[14], ptr[15]);

    strw = WIN_UTF8ToString(keystr);
    rc = (RegOpenKeyExW(HKEY_LOCAL_MACHINE, strw, 0, KEY_QUERY_VALUE, &hkey) == ERROR_SUCCESS);
    SDL_free(strw);
    if (!rc) {
        return WIN_StringToUTF8(name);  /* oh well. */
    }

    rc = (RegQueryValueExW(hkey, L"Name", NULL, NULL, NULL, &len) == ERROR_SUCCESS);
    if (!rc) {
        RegCloseKey(hkey);
        return WIN_StringToUTF8(name);  /* oh well. */
    }

    strw = (WCHAR *) SDL_malloc(len + sizeof (WCHAR));
    if (!strw) {
        RegCloseKey(hkey);
        return WIN_StringToUTF8(name);  /* oh well. */
    }

    rc = (RegQueryValueExW(hkey, L"Name", NULL, NULL, (LPBYTE) strw, &len) == ERROR_SUCCESS);
    RegCloseKey(hkey);
    if (!rc) {
        SDL_free(strw);
        return WIN_StringToUTF8(name);  /* oh well. */
    }

    strw[len / 2] = 0;  /* make sure it's null-terminated. */

    retval = WIN_StringToUTF8(strw);
    SDL_free(strw);
    return retval ? retval : WIN_StringToUTF8(name);
#endif /* if __WINRT__ / else */
}


static BOOL CALLBACK
dsound_enumerate_callback ( LPGUID guid, const wchar_t *name, LPCWSTR module, void *context )
{
    b32 is_capture_device = ( b32 ) context;
    if ( guid ) 
	{ 
        i8 *str = dsound_lookup_audio_device_name ( name , guid );
        if ( str ) 
		{
            GUID coppied_guid [ sizeof ( GUID ) ];
            memcpy ( coppied_guid, guid, sizeof  ( GUID ) );
		}
    }
    return true;  
}

static void dsound_detect_device ( void )
{
    DirectSoundCaptureEnumerateW ( dsound_enumerate_callback, ( void* ) ( ( b32 ) true  ) );
    DirectSoundEnumerateW        ( dsound_enumerate_callback, ( void* ) ( ( b32 ) false ) );
}
#endif

static void dsound_wait_device ( dsound_t *dsound )
{
    u32 status = 0;
    u32 cursor = 0;
    u32 junk   = 0;
    HRESULT hr = DS_OK;

	hr = IDirectSoundBuffer_GetCurrentPosition ( dsound->secondary_buffer, &junk, &cursor );
    if ( hr != DS_OK ) 
	{
        if ( hr == DSERR_BUFFERLOST ) 
		{
            CHECK_HRESULT ( IDirectSoundBuffer_Restore ( dsound->secondary_buffer ) );
        }
        return;
    }

    while ( ( cursor / dsound->size ) == dsound->last_chunk ) 
	{
        /* FIXME: find out how much time is left and sleep that long */
        // Sleep ( cursor * 1000 );	

        CHECK_HRESULT ( IDirectSoundBuffer_GetStatus ( dsound->secondary_buffer, &status ) );
        if ( ( status & DSBSTATUS_BUFFERLOST ) ) 
		{
			printf ( "lost\n" );
            IDirectSoundBuffer_Restore   ( dsound->secondary_buffer );
            IDirectSoundBuffer_GetStatus ( dsound->secondary_buffer, &status );
            if ( ( status & DSBSTATUS_BUFFERLOST ) ) 
			{
                break;
            }
        }
        if ( !( status & DSBSTATUS_PLAYING ) ) 
		{           
			CHECK_HRESULT ( IDirectSoundBuffer_Play ( dsound->secondary_buffer, 0, 0, DSBPLAY_LOOPING ) );
            return;
        }
        CHECK_HRESULT ( IDirectSoundBuffer_GetCurrentPosition ( dsound->secondary_buffer, &junk, &cursor ) );
    }
}

void dsound_play_device ( dsound_t *dsound )
{
    if ( dsound->locked_audio_buffer ) 
	{
        CHECK_HRESULT ( IDirectSoundBuffer_Unlock ( dsound->secondary_buffer,
													( void* ) dsound->locked_audio_buffer,
													dsound->size, 
													0, 
													0 ) );
    }
}

void dsound_stop_device ( dsound_t *dsound )
{
	if ( dsound->locked_audio_buffer ) 
	{
		CHECK_HRESULT ( IDirectSoundBuffer_Stop ( dsound->secondary_buffer ) );
	}
}

u8* dsound_get_device_buffer ( dsound_t *dsound )
{
    u32 cursor;
    u32 junk;
    HRESULT hr      = DS_OK;
    u32 length      = 0;
	void*  junk_ptr = 0;

    dsound->locked_audio_buffer = 0;

    hr = IDirectSoundBuffer_GetCurrentPosition ( dsound->secondary_buffer, &junk, &cursor );
    if ( hr == DSERR_BUFFERLOST ) 
	{
        CHECK_HRESULT ( IDirectSoundBuffer_Restore            ( dsound->secondary_buffer                 ) );
        CHECK_HRESULT ( IDirectSoundBuffer_GetCurrentPosition ( dsound->secondary_buffer, &junk, &cursor ) );
    }
    cursor             /= dsound->size;
    dsound->last_chunk  = cursor;
    cursor              = ( cursor + 1 ) % dsound->number_of_buffers;
    cursor             *= dsound->size;

    /* Lock the audio buffer */
    hr = IDirectSoundBuffer_Lock ( dsound->secondary_buffer, 
								   cursor, 
								   dsound->size,
                                   ( void** ) &dsound->locked_audio_buffer,
								   &length, 
								   ( void** ) &junk_ptr, 
								   &junk, 
		                           0 );							   
    if ( hr == DSERR_BUFFERLOST ) 
	{
        CHECK_HRESULT ( IDirectSoundBuffer_Restore ( dsound->secondary_buffer ) );
        CHECK_HRESULT ( IDirectSoundBuffer_Lock    ( dsound->secondary_buffer, 
													 cursor,
													 dsound->size,
													 ( void** ) &dsound->locked_audio_buffer, 
													 &length, 
													 ( void** ) &junk_ptr,
													 &junk, 
			                                         0 ) );
    }
    return ( dsound->locked_audio_buffer );
}

static int dsound_capture_from_device ( dsound_t *dsound, void *buffer, u32 length )
{
    u32 junk;
	u32 cursor;
	u32	audio_bytes1;
	u32 audio_bytes2;
    void *audio_ptr1;
    void *audio_ptr2;

    assert ( length == dsound->size );

    while ( 1 ) 
	{
        CHECK_HRESULT ( IDirectSoundCaptureBuffer_GetCurrentPosition ( dsound->capture_buffer, &junk, &cursor ) );
        if ( ( cursor / dsound->size ) == dsound->last_chunk ) 
		{
			Sleep ( 1000 );  /* FIXME: find out how much time is left and sleep that long */
        } 
		else 
		{
            break;
        }
    }

    CHECK_HRESULT ( IDirectSoundCaptureBuffer_Lock ( dsound->capture_buffer, 
													 dsound->last_chunk * dsound->size, 
													 dsound->size, 
													 ( void** ) &audio_ptr1, 
													 &audio_bytes1,
													 ( void** ) &audio_ptr2,
													 &audio_bytes2,
													 0 ) );

    assert ( audio_bytes1 == dsound->size  );
    assert ( audio_ptr2   == 0     );
    assert ( audio_bytes2 == 0     );

    memcpy ( buffer, audio_ptr1, audio_bytes1 );
    CHECK_HRESULT ( IDirectSoundCaptureBuffer_Unlock ( dsound->capture_buffer, 
													   ( void* ) audio_ptr1, 
													   audio_bytes1, 
													   ( void* ) audio_ptr2, 
													   audio_bytes2 ) );
													   
    dsound->last_chunk = ( dsound->last_chunk + 1 ) % dsound->number_of_buffers;
	
    return audio_bytes1;
}

static void dsound_flush_capture_buffer ( dsound_t *dsound )
{
    u32 junk   = 0;
	u32 cursor = 0;
    CHECK_HRESULT ( IDirectSoundCaptureBuffer_GetCurrentPosition ( dsound->capture_buffer, &junk, &cursor ) );
	dsound->last_chunk = cursor / dsound->size;
}

static void dsound_close_device ( dsound_t *dsound )
{
	if ( dsound->primary_buffer )
	{
        IDirectSoundBuffer_Stop ( dsound->primary_buffer );
        SAFE_RELEASE ( dsound->primary_buffer );
    }
    if ( dsound->secondary_buffer )
	{
        IDirectSoundBuffer_Stop ( dsound->secondary_buffer );
        SAFE_RELEASE ( dsound->secondary_buffer );
    }
    if ( dsound->playback ) 
	{
		SAFE_RELEASE ( dsound->playback );
    }
    if ( dsound->capture_buffer ) 
	{
        IDirectSoundCaptureBuffer_Stop ( dsound->capture_buffer );
		SAFE_RELEASE                   ( dsound->capture_buffer );
    }
    if ( dsound->capture )
	{
		SAFE_RELEASE ( dsound->capture );
    }
	
	DSOUND_UNLOAD ( DSOUND_DLL );
	return;
}


static b32 dsound_create_capture_buffer ( dsound_t *dsound, const u32 buffer_size, WAVEFORMATEX *wfx )
{
	u32     cursor              = 0;
	u32     junk                = 0; 
    dsound->dscbd.dwSize        = sizeof ( dsound->dscbd );
    dsound->dscbd.dwFlags       = DSCBCAPS_WAVEMAPPED;
    dsound->dscbd.dwBufferBytes = buffer_size;
    dsound->dscbd.lpwfxFormat   = wfx;

    CHECK_HRESULT ( IDirectSoundCapture_CreateCaptureBuffer ( dsound->capture, &dsound->dscbd, &dsound->capture_buffer, 0 ) );
    CHECK_HRESULT ( IDirectSoundCaptureBuffer_Start         ( dsound->capture_buffer, DSCBSTART_LOOPING  ) );

#if 0
    CHECK_HRESULT ( IDirectSoundCaptureBuffer_GetCurrentPosition ( dsound->capture_buffer, &junk, &cursor ) );
    dsound->last_chunk = cursor / dsound->size;
#endif
    return true;
}


b32 dsound_create_primary_buffer ( dsound_t *dsound )
{
	dsound->primary_dsbd.dwSize          = sizeof ( DSBUFFERDESC );
	dsound->primary_dsbd.dwFlags         = DSBCAPS_PRIMARYBUFFER;
	dsound->primary_dsbd.dwBufferBytes   = 0;
	dsound->primary_dsbd.dwReserved      = 0;
	dsound->primary_dsbd.lpwfxFormat     = 0;	
	CHECK_HRESULT ( IDirectSound_CreateSoundBuffer ( dsound->playback, 
													 &dsound->primary_dsbd, 
													 &dsound->primary_buffer, 
													 0 ) );
	return true;
}

static u32 dsound_create_secondary_buffer ( dsound_t *dsound, HWND hwnd, u32 buffer_size, WAVEFORMATEX* wfx )
{
    u32    num_chunks    = 8;
    void*  audio_ptr1    = 0;
	void*  audio_ptr2    = 0;
    u32    audio_bytes1  = 0;
	u32    audio_bytes2  = 0;

    if ( hwnd ) 
	{
        CHECK_HRESULT ( IDirectSound_SetCooperativeLevel ( dsound->playback, hwnd, DSSCL_PRIORITY ) );
    } 
	else 
	{
        CHECK_HRESULT ( IDirectSound_SetCooperativeLevel ( dsound->playback, GetDesktopWindow ( ), DSSCL_NORMAL ) );
    }
	
    dsound->secondary_dsbd.dwSize  = sizeof ( DSBUFFERDESC );
	dsound->secondary_dsbd.dwFlags = DSBCAPS_GETCURRENTPOSITION2 |
									 DSBCAPS_CTRLVOLUME;
	
    if ( !hwnd ) 
	{
        dsound->secondary_dsbd.dwFlags |= DSBCAPS_GLOBALFOCUS;
    } 
	else 
	{
        // dsound->secondary_dsbd.dwFlags |= DSBCAPS_STICKYFOCUS;
        dsound->secondary_dsbd.dwFlags |= DSBCAPS_GLOBALFOCUS;
    }
    dsound->secondary_dsbd.dwBufferBytes = buffer_size;
    if ( ( dsound->secondary_dsbd.dwBufferBytes < DSBSIZE_MIN ) || ( dsound->secondary_dsbd.dwBufferBytes > DSBSIZE_MAX ) ) 
	{
        fprintf ( stderr, "Sound buffer size must be between %d and %d", DSBSIZE_MIN / num_chunks, DSBSIZE_MAX / num_chunks );
    }
    dsound->secondary_dsbd.dwReserved  = 0;
    dsound->secondary_dsbd.lpwfxFormat = wfx;
    
	CHECK_HRESULT ( IDirectSound_CreateSoundBuffer ( dsound->playback, &dsound->secondary_dsbd, &dsound->secondary_buffer, 0 ) );
    // CHECK_HRESULT ( IDirectSoundBuffer_SetFormat   ( dsound->secondary_buffer, wfx ) );

    /* Silence the first audio buffer */
    CHECK_HRESULT ( IDirectSoundBuffer_Lock ( dsound->secondary_buffer, 
											  0, 
											  dsound->secondary_dsbd.dwBufferBytes,
											  ( void** ) &audio_ptr1, 
											  &audio_bytes1,
											  ( void** ) &audio_ptr2, 
											  &audio_bytes2,
											  DSBLOCK_ENTIREBUFFER ) );
    memset ( audio_ptr1, 0, audio_bytes1 );
	
	CHECK_HRESULT ( IDirectSoundBuffer_Unlock ( dsound->secondary_buffer,
												( void* ) audio_ptr1, 
												audio_bytes1,
												( void* ) audio_ptr2, 
												audio_bytes2 ) );

    return ( num_chunks );
}

dsound_t dsound_open_device  ( HWND hwnd,
							   u32 frequency,
							   u32 channels,
							   u32 bps,
							   b32 is_pcm,
							   b32 is_capture_device	)
{
	dsound_t dsound      = { 0 };
    u32 num_chunks       = 8;
    // LPGUID guid          = ( LPGUID ) handle;
	u32 buffer_size      = 0;
	dsound.size          = ( bps / 8 ) * channels * frequency ;
	b32  result          = false;
	
	DSOUND_LOAD ( DirectSoundCreate8        );
	DSOUND_LOAD ( DirectSoundCaptureCreate8 );
	
    if ( is_capture_device )
	{
        CHECK_HRESULT ( pDirectSoundCaptureCreate8 ( 0, &dsound.capture, 0 ) );
    } 

	CHECK_HRESULT ( pDirectSoundCreate8 ( 0, &dsound.playback, 0 ) );
    CHECK_HRESULT ( IDirectSound_SetCooperativeLevel ( dsound.playback, GetDesktopWindow ( ), DSSCL_NORMAL ) );
	
	// assert ( dsound_create_primary_buffer ( &dsound ) );
	
	buffer_size = num_chunks * dsound.size;
	
	if ( ( buffer_size < DSBSIZE_MIN ) || ( buffer_size > DSBSIZE_MAX ) ) 
	{
		fprintf ( stderr, "Sound buffer size must be between %d and %d",
                             ( u32 ) ( ( DSBSIZE_MIN < num_chunks ) ? 1 : DSBSIZE_MIN / num_chunks ),
                             ( u32 )   ( DSBSIZE_MAX / num_chunks ) );
	} 
	else 
	{
        if ( !is_pcm ) 
		{
            dsound.wfx_playback.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
		} 
		else 
		{
			dsound.wfx_playback.wFormatTag = WAVE_FORMAT_PCM;
        }
       
		dsound.wfx_playback.wBitsPerSample  = bps;
        dsound.wfx_playback.nChannels       = channels;
        dsound.wfx_playback.nSamplesPerSec  = frequency;
        dsound.wfx_playback.nBlockAlign     = dsound.wfx_playback.nChannels * ( dsound.wfx_playback.wBitsPerSample / 8 );
        dsound.wfx_playback.nAvgBytesPerSec = dsound.wfx_playback.nSamplesPerSec * dsound.wfx_playback.nBlockAlign;

        if ( is_capture_device )
		{
			dsound.wfx_capture.wFormatTag      = is_pcm ? WAVE_FORMAT_PCM : WAVE_FORMAT_IEEE_FLOAT;
			dsound.wfx_capture.wBitsPerSample  = bps;
			dsound.wfx_capture.nChannels       = channels;
			dsound.wfx_capture.nSamplesPerSec  = frequency;
			dsound.wfx_capture.nBlockAlign     = dsound.wfx_capture.nChannels * ( dsound.wfx_capture.wBitsPerSample / 8 );
			dsound.wfx_capture.nAvgBytesPerSec = dsound.wfx_capture.nSamplesPerSec * dsound.wfx_capture.nBlockAlign;

			dsound_create_capture_buffer   ( &dsound, buffer_size, &dsound.wfx_capture );
		}
		
		dsound_create_secondary_buffer ( &dsound, hwnd, buffer_size, &dsound.wfx_playback );
		dsound.number_of_buffers = num_chunks;
	}
	
    return dsound;  
}

#if 0
void dsound_play_note ( dsound_t *ds, f32 frequency )
{
	void *buffer  = 0;
	i16  *p       = 0;
	i16   output  = 0;
	DWORD length  = 0;
	
	IDirectSoundBuffer_Stop ( ds->dsound_playback_buffer );

	if ( IDirectSoundBuffer_Lock ( ds->dsound_playback_buffer, 
								   0, 
								   ds->dsbd.dwBufferBytes, 
								   &buffer, 
								   &length, 
								   0, 
								   0, 
								   DSBLOCK_ENTIREBUFFER ) == DS_OK )
	{
		p                   = ( i16* ) buffer;
		f32 duration        = 0.38f    * SAMPLERATE;
		f32 attack_time     = 0.0001f  * SAMPLERATE;
		f32 decay_time      = 0.1f     * SAMPLERATE;
		f32 decay_start     = duration - decay_time;
		f32 peak_amplitude  = 1.0f;
		f32 start_amplitude = 0.0f;
		f32 end_amplitude   = 0.0f;
		f32 volume          = start_amplitude;
		f32 envInc          = ( peak_amplitude - start_amplitude ) / attack_time;

		for ( i32 i = 0; i < SAMPLERATE; i++ )
		{
			if ( i < ( i32 ) duration )
			{
				if ( i < attack_time || i > decay_start )
				{
					volume += envInc;
				}
				
				else if ( i == decay_start )
				{
					envInc = end_amplitude - volume;
					if ( decay_time > 0 )
					{
						envInc /= decay_time;
					}
				}
				else
				{
					volume = peak_amplitude;
				}
				
				//output = ( ( f32 ) SINE_TABLE [ phase >> 8 ] * volume );
				output = ( ( f32 ) COSINE_TABLE [ phase >> 8 ] * volume );
				phase += ( 65535.0f * frequency / SAMPLERATE );

				p [ 0 ] = output;
				p [ 1 ] = output;
				p += 2;
			}
			else
			{
				p [ 0 ] = 0;
				p [ 1 ] = 0;
				p += 2;
			}
		}
		IDirectSoundBuffer_Unlock ( ds->dsound_playback_buffer, ( void* ) buffer , length , 0 , 0 );
	}
	IDirectSoundBuffer_SetCurrentPosition ( ds->dsound_playback_buffer, 0 );
	IDirectSoundBuffer_Play               ( ds->dsound_playback_buffer, 0 , 0 , 0 );
	
	return;
}
#endif


#ifdef __cplusplus
}
#endif // !__cplusplus


#endif // !__DIRECTSOUND_H__