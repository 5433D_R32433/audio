#ifndef __WAVE_H__
#define __WAVE_H__


#ifdef __cplusplus
extern "C" {
#endif // !__cplusplus

#if defined(_MSC_VER) && (defined(_M_X64) || defined(_M_AMD64) || defined(_M_IX86))
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif


#if !defined(__5433D_HELPERS__)
#define __5433D_HELPERS__

#define LITTLE_ENDIAN_BYTE_ORDER 0x41424344UL 
#define BIG_ENDIAN_BYTE_ORDER    0x44434241UL
#define PDP_ENDIAN_BYTE_ORDER    0x42414443UL
#define BYTE_ORDER               ( ( ( 'A' << 24 ) | ( 'B' << 16) | ( 'C' << 8 ) | 'D' ) ) 

#if BYTE_ORDER==LITTLE_ENDIAN_BYTE_ORDER
#define LITTLE_ENDIAN
#elif BYTE_ORDER==BIG_ENDIAN_BYTE_ORDER
#define BIG_ENDIAN
#elif BYTE_ORDER__==__PDP_ENDIAN_BYTE_ORDER
#define PDP_ENDIAN
#else
#error "Unknown Bytes Order Endianness"
#endif


#define likely(p)   (!!(p))
#define unlikely(p) (!!(p))


#if defined(_WIN32) && !defined(_WIN64)
typedef long long          i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#elif defined(_WIN64)
typedef __int64            i64;
typedef __int32            i32;
typedef __int16            i16;
typedef __int8              i8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef long int           i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#endif


#ifdef _WIN32
typedef signed long long   s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#elif defined(_WIN64)
typedef signed __int64     s64;
typedef signed __int32     s32;
typedef signed __int16     s16;
typedef signed __int8       s8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef signed long int    s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#endif


#if defined(_WIN32)
typedef unsigned long long   u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#elif defined(_WIN64)
typedef unsigned __int64     u64;
typedef unsigned __int32     u32;
typedef unsigned __int16     u16;
typedef unsigned __int8       u8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef unsigned long int    u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#endif

#if defined(_WIN64)
#define usizemax 0xffffffffffffffffui64
typedef i64 imax;
typedef u64 umax;
typedef u64 usize;
typedef i64 isize;
typedef i64 ptrdiff;
typedef i64 iptr;
typedef u64 uptr;
#elif defined(_WIN32) && !defined(_WIN64)
#define usizemax 0xffffffffui32
typedef i32      imax;
typedef u32      umax;
typedef u32      usize;
typedef i32      isize;
typedef i32      ptrdiff;
typedef i32      iptr;
typedef u32      uptr;
#elif defined(unix) || defined(UNIX) || defined(__unix__) || defined(__UNIX__) || defined(__BSD__) || defined(BSD) || defined(__FreeBSD__) || defined(__NetBSD__) || defined (__OpenBSD__) || defined(__LINUX__)

#elif defined(__APPLE__) && defined(__MACH__)

#endif 

typedef float       f32; 
typedef long double f64;    

typedef i32 b32;
typedef i16 b16;
typedef i8  b8;

#define i8min        (-127i8 - 1)
#define i16min       (-32767i16 - 1)
#define i32min       (-2147483647i32 - 1)
#define i64min       (-9223372036854775807i64 - 1)
#define i8max        127i8
#define i16max       32767i16
#define i32max       2147483647i32
#define i64max       9223372036854775807i64
#define u8max        0xffui8
#define u16max       0xffffui16
#define u32max       0xffffffffui32
#define u64max       0xffffffffffffffffui64


#if !defined(__cplusplus)
#define true  1
#define false 0
#endif

#if defined(__GNUC__) || defined(__clang__)
#define __FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define __FORCEINLINE __forceinline
#else
#define __FORCEINLINE inline
#endif

#ifdef _MSC_VER
# define __PACKED(d) __pragma(pack(push, 1)) d __pragma(pack(pop))
# define __PACKED_START __pragma(pack(push, 1))
# define __PACKED_END   __pragma(pack(pop))
#elif defined (__GNUC__) || defined (__clang__)
# define __PACKED(d) d __attribute__((packed, aligned(1)))
# define __PACKED_START _Pragma("pack(push, 1)")
# define __PACKED_END   _Pragma("pack(pop)")
#endif


#define FREENULL(p) do { free( p ); p = 0; } while(0)
#define EMPTY_STR(str) (!str || !*str)

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b))
#endif // !MIN

#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b))
#endif // !MAX

#define UNUSED(v)        ( ( void ) v )
#define ARRAY_COUNT( a ) ( sizeof ( a ) / sizeof ( ( a ) [ 0 ] ) )

#define ALIGN_DOWN(x,a)      ( ( x ) & ~( ( a ) - 1 ) )
#define ALIGN_UP(x,a)        ALIGN_DOWN ( ( x ) + ( a ) - 1, ( a ) )
#define ALIGN_DOWN_PTR(p, a) ((void *)ALIGN_DOWN((uptr)(p), (a)))
#define ALIGN_UP_PTR(p, a)   ((void *)ALIGN_UP((uptr)(p), (a)))

	
#if defined(_MSC_VER)
__FORCEINLINE u16 BYTESWAP16 ( u16 x ) { return _byteswap_ushort ( x ); }
__FORCEINLINE u32 BYTESWAP32 ( u32 x ) { return _byteswap_ulong  ( x ); }
__FORCEINLINE u64 BYTESWAP64 ( u64 x ) { return _byteswap_uint64 ( x ); }
#elif defined (__GNUC__) || defined (__clang__)
__FORCEINLINE u16 BYTESWAP16 ( u16 x ) { return __builtin_bswap16 ( x ); }
__FORCEINLINE u32 BYTESWAP32 ( u32 x ) { return __builtin_bswap32 ( x ); }
__FORCEINLINE u64 BYTESWAP64 ( u64 x ) { return __builtin_bswap64 ( x ); }
#else
#define BYTESWAP16(x) ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define BYTESWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )  

#define BYTESWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )	
#endif

#define KB(x) (      x   * 1024 )
#define MB(x) ( KB ( x ) * 1024 )
#define GB(x) ( MB ( x ) * 1024 )
#define TB(x) ( GB ( x ) * 1024 )
#define PB(x) ( TB ( x ) * 1024 )

#else
	
#endif // !__5433D_HELPERS__


#include <assert.h>


#define RIFF_MAGIC ( 'R' | ( 'I' << 8 ) | ( 'F' << 16 ) | ( 'F' << 24 ) )
#define WAVE_MAGIC ( 'W' | ( 'A' << 8 ) | ( 'V' << 16 ) | ( 'E' << 24 ) )
#define FMT_MAGIC  ( 'f' | ( 'm' << 8 ) | ( 't' << 16 ) | ( ' ' << 24 ) )
#define DATA_MAGIC ( 'd' | ( 'a' << 8 ) | ( 't' << 16 ) | ( 'a' << 24 ) )
#define LIST_MAGIC ( 'L' | ( 'I' << 8 ) | ( 'S' << 16 ) | ( 'T' << 24 ) )
#define IARL_MAGIC ( 'I' | ( 'A' << 8 ) | ( 'R' << 16 ) | ( 'L' << 24 ) )
#define IART_MAGIC ( 'I' | ( 'A' << 8 ) | ( 'R' << 16 ) | ( 'T' << 24 ) )
#define ICMS_MAGIC ( 'I' | ( 'C' << 8 ) | ( 'M' << 16 ) | ( 'S' << 24 ) )
#define ICMT_MAGIC ( 'I' | ( 'C' << 8 ) | ( 'M' << 16 ) | ( 'T' << 24 ) )
#define ICOP_MAGIC ( 'I' | ( 'C' << 8 ) | ( 'O' << 16 ) | ( 'P' << 24 ) )
#define ICRD_MAGIC ( 'I' | ( 'C' << 8 ) | ( 'R' << 16 ) | ( 'D' << 24 ) )
#define ICRP_MAGIC ( 'I' | ( 'C' << 8 ) | ( 'R' << 16 ) | ( 'P' << 24 ) )
#define IDIM_MAGIC ( 'I' | ( 'D' << 8 ) | ( 'I' << 16 ) | ( 'M' << 24 ) )
#define IDPI_MAGIC ( 'I' | ( 'D' << 8 ) | ( 'P' << 16 ) | ( 'I' << 24 ) )
#define IENG_MAGIC ( 'I' | ( 'E' << 8 ) | ( 'N' << 16 ) | ( 'G' << 24 ) )
#define IGNR_MAGIC ( 'I' | ( 'G' << 8 ) | ( 'N' << 16 ) | ( 'R' << 24 ) )
#define IKEY_MAGIC ( 'I' | ( 'K' << 8 ) | ( 'E' << 16 ) | ( 'Y' << 24 ) )
#define ILGT_MAGIC ( 'I' | ( 'L' << 8 ) | ( 'G' << 16 ) | ( 'T' << 24 ) )
#define IMED_MAGIC ( 'I' | ( 'M' << 8 ) | ( 'E' << 16 ) | ( 'D' << 24 ) )
#define INAM_MAGIC ( 'I' | ( 'N' << 8 ) | ( 'A' << 16 ) | ( 'M' << 24 ) )
#define IPLT_MAGIC ( 'I' | ( 'P' << 8 ) | ( 'L' << 16 ) | ( 'T' << 24 ) )
#define IPRD_MAGIC ( 'I' | ( 'P' << 8 ) | ( 'R' << 16 ) | ( 'D' << 24 ) )
#define ISBJ_MAGIC ( 'I' | ( 'S' << 8 ) | ( 'B' << 16 ) | ( 'J' << 24 ) )
#define ISFT_MAGIC ( 'I' | ( 'S' << 8 ) | ( 'F' << 16 ) | ( 'T' << 24 ) )
#define ISRC_MAGIC ( 'I' | ( 'S' << 8 ) | ( 'R' << 16 ) | ( 'C' << 24 ) )
#define ISRF_MAGIC ( 'I' | ( 'S' << 8 ) | ( 'R' << 16 ) | ( 'F' << 24 ) )
#define ITCH_MAGIC ( 'I' | ( 'T' << 8 ) | ( 'C' << 16 ) | ( 'H' << 24 ) )

/*
 *  IARL 	The location where the subject of the file is archived
 *  IART 	The artist of the original subject of the file
 *  ICMS 	The name of the person or organization that commissioned the original subject of the file
 *  ICMT 	General comments about the file or its subject
 *	ICOP 	Copyright information about the file (e.g., "Copyright Some Company 2011")
 *	ICRD 	The date the subject of the file was created (creation date) (e.g., "2022-12-31")
 *	ICRP 	Whether and how an image was cropped
 *	IDIM 	The dimensions of the original subject of the file
 *	IDPI 	Dots per inch settings used to digitize the file
 *	IENG 	The name of the engineer who worked on the file
 *	IGNR 	The genre of the subject
 *	IKEY 	A list of keywords for the file or its subject
 *	ILGT 	Lightness settings used to digitize the file
 *	IMED 	Medium for the original subject of the file
 *	INAM 	Title of the subject of the file (name)
 *	IPLT 	The number of colors in the color palette used to digitize the file
 *	IPRD 	Name of the title the subject was originally intended for
 *	ISBJ 	Description of the contents of the file (subject)
 *	ISFT 	Name of the software package used to create the file
 *	ISRC 	The name of the person or organization that supplied the original subject of the file
 *	ISRF 	The original form of the material that was digitized (source form)
 *	ITCH    The name of the technician who digitized the subject file
 */

u8 WAVE_BUFFER [ MB ( 256 ) ] = { 0 };

typedef struct wave_header_t
{
	u32 riff_magic;
	u32 size;
	u32 wave_magic;
} wave_header_t;


typedef struct wave_chunk_t
{
	u32 id;
	u32 size; 
} wave_chunk_t;

typedef struct wave_list_t
{
	int x;
} wave_list_t;


typedef struct wave_fmt_t
{
	u16 format_tag;
    u16 channels;
    u32 samples_per_secs;
    u32 avg_bytes_per_sec;
	u16 block_align;
    u16 bits_per_sample;
} wave_fmt_t;

typedef struct wave_data_t
{
	int x;
} wave_data_t;


void wave_load ( const i8 *file_name )
{
	FILE *fp = fopen ( file_name, "rb" );
	assert ( fp );
	fseek ( fp, 0, SEEK_END );
	u64 size = ftell ( fp );
	fseek ( fp, 0, SEEK_SET );
	assert ( fread ( WAVE_BUFFER, 1, size, fp ) == size );
	u8 *p = ( u8* ) WAVE_BUFFER;
	wave_header_t *h = ( wave_header_t* ) p;
	assert ( h->riff_magic == RIFF_MAGIC );
	assert ( h->wave_magic == WAVE_MAGIC );
	wave_chunk_t *chnk = ( wave_chunk_t* ) ( p + sizeof ( wave_header_t ) );
	
	while ( ( u8* ) chnk < ( p + h->size + 8 ) )
	{
		switch ( chnk->id )
		{
			case FMT_MAGIC:
			{
				wave_fmt_t* fmt = ( wave_fmt_t* ) ( ( u8* ) chnk + 8 );
				chnk = ( wave_chunk_t* ) ( ( u8* ) fmt + chnk->size );
			} break;

			case LIST_MAGIC:
			{
				wave_list_t *l = ( wave_list_t* ) ( ( u8* ) chnk + 8 );
				chnk = ( wave_chunk_t* ) ( ( u8* ) l + chnk->size );
			} break;

			case DATA_MAGIC:
			{
				wave_data_t *d = ( wave_data_t* ) ( ( u8* ) chnk + 8 );
				chnk = ( wave_chunk_t* ) ( ( u8* ) d + chnk->size );
			} break;

			default:
			{
			} break;
		}
	}

    return;
}


#ifdef __cplusplus
}
#endif

#endif // !__WAVE_H__




  /*
  'monophonic (mono)'
  'stereo (left, right)'
  'linear surround (front center, front left, front right)'
  'quadraphonic (front center, front left, front right, rear center)'
  '5.0 surround (front center, front left, front right, rear left, rear right)'
  '5.1 surround (front center, front left, front right, rear left, rear right, LFE)'
  '7.1 surround (front center, front left, front right, side left, side right, rear left, rear right, LFE)'
  */