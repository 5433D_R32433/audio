#ifndef __MP4_H__
#define __MP4_H__

#ifdef __cplusplus
extern "C" {
#endif // !__cplusplus

#if defined(_MSC_VER) && (defined(_M_X64) || defined(_M_AMD64) || defined(_M_IX86))
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif

#if !defined(__5433D_HELPERS__)
#define __5433D_HELPERS__

#define LITTLE_ENDIAN_BYTE_ORDER 0x41424344UL 
#define BIG_ENDIAN_BYTE_ORDER    0x44434241UL
#define PDP_ENDIAN_BYTE_ORDER    0x42414443UL
#define BYTE_ORDER               ( ( ( 'A' << 24 ) | ( 'B' << 16) | ( 'C' << 8 ) | 'D' ) ) 

#if BYTE_ORDER==LITTLE_ENDIAN_BYTE_ORDER
#define LITTLE_ENDIAN
#elif BYTE_ORDER==BIG_ENDIAN_BYTE_ORDER
#define BIG_ENDIAN
#elif BYTE_ORDER__==__PDP_ENDIAN_BYTE_ORDER
#define PDP_ENDIAN
#else
#error "Unknown Bytes Order Endianness"
#endif


#define likely(p)   (!!(p))
#define unlikely(p) (!!(p))


#if defined(_WIN32) && !defined(_WIN64)
typedef long long          i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#elif defined(_WIN64)
typedef __int64            i64;
typedef __int32            i32;
typedef __int16            i16;
typedef __int8              i8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef long int           i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#endif


#ifdef _WIN32
typedef signed long long   s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#elif defined(_WIN64)
typedef signed __int64     s64;
typedef signed __int32     s32;
typedef signed __int16     s16;
typedef signed __int8       s8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef signed long int    s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#endif


#if defined(_WIN32)
typedef unsigned long long   u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#elif defined(_WIN64)
typedef unsigned __int64     u64;
typedef unsigned __int32     u32;
typedef unsigned __int16     u16;
typedef unsigned __int8       u8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef unsigned long int    u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#endif

#if defined(_WIN64)
#define usizemax 0xffffffffffffffffui64
typedef i64 imax;
typedef u64 umax;
typedef u64 usize;
typedef i64 isize;
typedef i64 ptrdiff;
typedef i64 iptr;
typedef u64 uptr;
#elif defined(_WIN32) && !defined(_WIN64)
#define usizemax 0xffffffffui32
typedef i32      imax;
typedef u32      umax;
typedef u32      usize;
typedef i32      isize;
typedef i32      ptrdiff;
typedef i32      iptr;
typedef u32      uptr;
#elif defined(unix) || defined(UNIX) || defined(__unix__) || defined(__UNIX__) || defined(__BSD__) || defined(BSD) || defined(__FreeBSD__) || defined(__NetBSD__) || defined (__OpenBSD__) || defined(__LINUX__)

#elif defined(__APPLE__) && defined(__MACH__)

#endif 


typedef float       f32; 
typedef long double f64;    

typedef i32 b32;
typedef i16 b16;
typedef i8  b8;

#define i8min        (-127i8 - 1)
#define i16min       (-32767i16 - 1)
#define i32min       (-2147483647i32 - 1)
#define i64min       (-9223372036854775807i64 - 1)
#define i8max        127i8
#define i16max       32767i16
#define i32max       2147483647i32
#define i64max       9223372036854775807i64
#define u8max        0xffui8
#define u16max       0xffffui16
#define u32max       0xffffffffui32
#define u64max       0xffffffffffffffffui64


#if !defined(__cplusplus)
#define true  1
#define false 0
#endif

#if defined(__GNUC__) || defined(__clang__)
#define __FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define __FORCEINLINE __forceinline
#else
#define __FORCEINLINE inline
#endif

#ifdef _MSC_VER
# define __PACKED(d) __pragma(pack(push, 1)) d __pragma(pack(pop))
# define __PACKED_START __pragma(pack(push, 1))
# define __PACKED_END   __pragma(pack(pop))
#elif defined (__GNUC__) || defined (__clang__)
# define __PACKED(d) d __attribute__((packed, aligned(1)))
# define __PACKED_START _Pragma("pack(push, 1)")
# define __PACKED_END   _Pragma("pack(pop)")
#endif


#define FREENULL(p) do { free( p ); p = 0; } while(0)
#define EMPTY_STR(str) (!str || !*str)

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b))
#endif // !MIN

#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b))
#endif // !MAX

#define UNUSED(v)        ( ( void ) v )
#define ARRAY_COUNT( a ) ( sizeof ( a ) / sizeof ( ( a ) [ 0 ] ) )

#define ALIGN_DOWN(x,a)      ( ( x ) & ~( ( a ) - 1 ) )
#define ALIGN_UP(x,a)        ALIGN_DOWN ( ( x ) + ( a ) - 1, ( a ) )
#define ALIGN_DOWN_PTR(p, a) ((void *)ALIGN_DOWN((uptr)(p), (a)))
#define ALIGN_UP_PTR(p, a)   ((void *)ALIGN_UP((uptr)(p), (a)))

	
#if defined(_MSC_VER)
__FORCEINLINE uint16_t BYTESWAP16 ( uint16_t x ) { return _byteswap_ushort ( x ); }
__FORCEINLINE uint32_t BYTESWAP32 ( uint32_t x ) { return _byteswap_ulong  ( x ); }
__FORCEINLINE uint64_t BYTESWAP64 ( uint64_t x ) { return _byteswap_uint64 ( x ); }
#elif defined (__GNUC__) || defined (__clang__)
__FORCEINLINE uint16_t BYTESWAP16 ( uint16_t x ) { return __builtin_bswap16 ( x ); }
__FORCEINLINE uint32_t BYTESWAP32 ( uint32_t x ) { return __builtin_bswap32 ( x ); }
__FORCEINLINE uint64_t BYTESWAP64 ( uint64_t x ) { return __builtin_bswap64 ( x ); }
#else
#define BYTESWAP16(x) ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define BYTESWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )  

#define BYTESWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )	
#endif

#define KB(x) (      x   * 1024 )
#define MB(x) ( KB ( x ) * 1024 )
#define GB(x) ( MB ( x ) * 1024 )
#define TB(x) ( GB ( x ) * 1024 )
#define PB(x) ( TB ( x ) * 1024 )

#else
	
#endif // !__5433D_HELPERS__


#define COBJMACROS
#include <Windows.h>
#include <mmdeviceapi.h>
#include <audioclient.h>
#include <avrt.h>
#include <stdio.h>
#include <math.h>

#pragma comment(lib, "ole32")
#pragma comment(lib, "avrt")

typedef struct wasapi_t
{
  IAudioClient*        output_client;
  IAudioClient*        input_client;
  IAudioRenderClient*  audio_render_client;
  IAudioCaptureClient* audio_capture_client;
  WAVEFORMATEX*        inwfx;
  WAVEFORMATEX*        outwfx;
  
} wasapi_t;















#ifdef __cplusplus
}
#endif // !__cplusplus
#endif // !__MP4_H__