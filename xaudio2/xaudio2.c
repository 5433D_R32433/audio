#include "xaudio2.h"
#define DR_WAV_IMPLEMENTATION
#include "dr_wav.h"

#include <time.h>
#include <stdio.h>

i32 main ( i32 argc, i8** argv )
{
	u32 channels;
    u32 frequency;
    u64 frame_count;
	u8* p = 0;
	xaudio2_t xa2 = { 0 };
	clock_t start, end;
	double t = 0;
    i16* data = drwav_open_file_and_read_pcm_frames_s16 ( "out.wav", &channels, &frequency, &frame_count, 0 );
    if ( data ) 
	{
		u32 size = ( frame_count * channels * 2 );
		p = ( u8* ) data;
		xa2 = xaudio2_open_device ( frequency, channels, 16, true );
		start = clock ( );
		
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		int columns, rows;

		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
		columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
		rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
		
		while ( p < ( ( u8* ) data + size ) )
		{
			xaudio2_wait_device ( &xa2 );
					
			// xaudio2_wait_done   ( &xa2 );			
			p += xa2.length;	
			memcpy ( xa2.buffer, p, xa2.length );
			memcpy ( xa2.buffer + xa2.length, p, xa2.length );
			xaudio2_play_device ( &xa2 );
			
			end = clock ( );
			
			t = ( ( double ) ( end - start ) / CLOCKS_PER_SEC );
#if 0		
		if ( t >= 5 )
			{
				printf ( "%c", 219 );
				// start = end;
			}
#endif
			f64 secs = ( f64 ) ( size / ( frequency * channels * 2 ) );
			
			printf ( "\rtime: %.01f of %.01f (%02d:%.01f)", t, secs, ( u32 ) secs / 60, ( f64 ) (( u32 ) secs % 60) );
			fflush ( stdout );
			// A - Green Foreground
			system ( "color 07" );
			
		}
    }
	
	
	drwav_free ( data, 0 );
	xaudio2_close_device ( &xa2 );
	
	system ( "pause" );
	UNUSED ( argc );
	UNUSED ( argv );
	return 0;
}