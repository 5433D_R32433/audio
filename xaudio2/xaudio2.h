#ifndef __XAUDIO2_H__
#define __XAUDIO2_H__

#ifdef __cplusplus
extern "C" {
#endif // !__cplusplus

#if defined(_MSC_VER) && (defined(_M_X64) || defined(_M_AMD64) || defined(_M_IX86))
#include <intrin.h>
#elif defined(__GNUC__) && (defined(__x86_64__) || defined(__i386__))
#include <x86intrin.h>
#endif


#if !defined(__5433D_HELPERS__)
#define __5433D_HELPERS__

#ifdef _WIN32
#include <Windows.h>
#define DBGPRINTF( ... ) { i8 buffer [ 512 ] = { 0 }; snprintf ( buffer, sizeof ( buffer ), __VA_ARGS__ ); OutputDebugString ( buffer ); }
#else
#define DBGPRINTF( ... ) { fprintf ( stderr, __VA_ARGS__ ); }
#endif

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET( x ) ( ( u8* ) 0 + ( x ) )
#endif

#define LITTLE_ENDIAN_BYTE_ORDER 0x41424344UL 
#define BIG_ENDIAN_BYTE_ORDER    0x44434241UL
#define PDP_ENDIAN_BYTE_ORDER    0x42414443UL
#define BYTE_ORDER               ( ( ( 'A' << 24 ) | ( 'B' << 16) | ( 'C' << 8 ) | 'D' ) ) 

#if BYTE_ORDER==LITTLE_ENDIAN_BYTE_ORDER
#define LITTLE_ENDIAN
#elif BYTE_ORDER==BIG_ENDIAN_BYTE_ORDER
#define BIG_ENDIAN
#elif BYTE_ORDER__==__PDP_ENDIAN_BYTE_ORDER
#define PDP_ENDIAN
#else
#error "Unknown Bytes Order Endianness"
#endif


#define likely(p)   (!!(p))
#define unlikely(p) (!!(p))


#if defined(_WIN32) && !defined(_WIN64)
typedef long long          i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#elif defined(_WIN64)
typedef __int64            i64;
typedef __int32            i32;
typedef __int16            i16;
typedef __int8              i8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef long int           i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#endif


#ifdef _WIN32
typedef signed long long   s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#elif defined(_WIN64)
typedef signed __int64     s64;
typedef signed __int32     s32;
typedef signed __int16     s16;
typedef signed __int8       s8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef signed long int    s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#endif


#if defined(_WIN32)
typedef unsigned long long   u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#elif defined(_WIN64)
typedef unsigned __int64     u64;
typedef unsigned __int32     u32;
typedef unsigned __int16     u16;
typedef unsigned __int8       u8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef unsigned long int    u64;
typedef unsigned int         u32;
typedef unsigned short       u16;
typedef unsigned char         u8;
#endif

#if defined(_WIN64)
#define usizemax 0xffffffffffffffffui64
typedef i64 imax;
typedef u64 umax;
typedef u64 usize;
typedef i64 isize;
typedef i64 ptrdiff;
typedef i64 iptr;
typedef u64 uptr;
#elif defined(_WIN32) && !defined(_WIN64)
#define usizemax 0xffffffffui32
typedef i32      imax;
typedef u32      umax;
typedef u32      usize;
typedef i32      isize;
typedef i32      ptrdiff;
typedef i32      iptr;
typedef u32      uptr;
#elif defined(unix) || defined(UNIX) || defined(__unix__) || defined(__UNIX__) || defined(__BSD__) || defined(BSD) || defined(__FreeBSD__) || defined(__NetBSD__) || defined (__OpenBSD__) || defined(__LINUX__)

#elif defined(__APPLE__) && defined(__MACH__)

#endif 

typedef float       f32; 
typedef long double f64;    

typedef i32 b32;
typedef i16 b16;
typedef i8  b8;

#define i8min        (-127i8 - 1)
#define i16min       (-32767i16 - 1)
#define i32min       (-2147483647i32 - 1)
#define i64min       (-9223372036854775807i64 - 1)
#define i8max        127i8
#define i16max       32767i16
#define i32max       2147483647i32
#define i64max       9223372036854775807i64
#define u8max        0xffui8
#define u16max       0xffffui16
#define u32max       0xffffffffui32
#define u64max       0xffffffffffffffffui64


#if !defined(__cplusplus)
#define true  1
#define false 0
#endif

#if defined(__GNUC__) || defined(__clang__)
#define __FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define __FORCEINLINE __forceinline
#else
#define __FORCEINLINE inline
#endif

#ifdef _MSC_VER
# define __PACKED(d) __pragma(pack(push, 1)) d __pragma(pack(pop))
# define __PACKED_START __pragma(pack(push, 1))
# define __PACKED_END   __pragma(pack(pop))
#elif defined (__GNUC__) || defined (__clang__)
# define __PACKED(d) d __attribute__((packed, aligned(1)))
# define __PACKED_START _Pragma("pack(push, 1)")
# define __PACKED_END   _Pragma("pack(pop)")
#endif


#define FREENULL(p) do { free( p ); p = 0; } while(0)
#define EMPTY_STR(str) (!str || !*str)

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b))
#endif // !MIN

#ifndef MAX
#define MAX(a,b) ( (a) > (b) ? (a) : (b))
#endif // !MAX

#define UNUSED(v)        ( ( void ) v )
#define ARRAY_COUNT( a ) ( sizeof ( a ) / sizeof ( ( a ) [ 0 ] ) )

#define ALIGN_DOWN(x,a)      ( ( x ) & ~( ( a ) - 1 ) )
#define ALIGN_UP(x,a)        ALIGN_DOWN ( ( x ) + ( a ) - 1, ( a ) )
#define ALIGN_DOWN_PTR(p, a) ((void *)ALIGN_DOWN((uptr)(p), (a)))
#define ALIGN_UP_PTR(p, a)   ((void *)ALIGN_UP((uptr)(p), (a)))

	
#if defined(_MSC_VER)
__FORCEINLINE u16 BYTESWAP16 ( u16 x ) { return _byteswap_ushort ( x ); }
__FORCEINLINE u32 BYTESWAP32 ( u32 x ) { return _byteswap_ulong  ( x ); }
__FORCEINLINE u64 BYTESWAP64 ( u64 x ) { return _byteswap_uint64 ( x ); }
#elif defined (__GNUC__) || defined (__clang__)
__FORCEINLINE u16 BYTESWAP16 ( u16 x ) { return __builtin_bswap16 ( x ); }
__FORCEINLINE u32 BYTESWAP32 ( u32 x ) { return __builtin_bswap32 ( x ); }
__FORCEINLINE u64 BYTESWAP64 ( u64 x ) { return __builtin_bswap64 ( x ); }
#else
#define BYTESWAP16(x) ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define BYTESWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )  

#define BYTESWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )	
#endif

#define KB(x) (      x   * 1024 )
#define MB(x) ( KB ( x ) * 1024 )
#define GB(x) ( MB ( x ) * 1024 )
#define TB(x) ( GB ( x ) * 1024 )
#define PB(x) ( TB ( x ) * 1024 )

#else
	
#endif // !__5433D_HELPERS__


#include <xaudio2.h>
#include <assert.h>

#define SAFE_COM_RELEASE(p)       \
if ( p )                          \
{                      		      \
	( p )->lpVtbl->Release ( p ); \
	( p ) = 0;                    \
}

#define SAFE_HANDLE_RELEASE(h)    \
if ( h )                          \
{								  \
	CloseHandle ( ( h ) );        \
	( h ) = 0;                    \
}

#define SR_MALLOC   malloc
#define SR_REALLOC  realloc
#define SR_CALLOC   calloc
#define SR_FREE     free

#define SAFE_PTR_RELEASE(p)    \
	if ( p )                   \
	{                          \
		SR_FREE ( p );         \
		( p ) = 0;             \
	}
	

#define CHECK_HRESULT(hr)                                                                                          \
	if ( FAILED ( hr ) )                                                                                           \
	{                                                                                                              \
	    char text    [ 2048  ] = { 0 };                                                                            \
	    char caption [ 2048  ] = { 0 };                                                                            \
        int  count  = 0;                                                                                           \
        snprintf ( text, 2048, "File: %s\nFuncion: %s\nLine: %d\n", __FILE__, __FUNCTION__, __LINE__ );            \
        count += snprintf ( caption, 2048, "0x%X: ", hr );                                                         \
		FormatMessage ( FORMAT_MESSAGE_FROM_SYSTEM |                                                               \
                        FORMAT_MESSAGE_IGNORE_INSERTS,                                                             \
                        0,                                                                                         \
                        hr,                                                                                        \
                        MAKELANGID ( LANG_NEUTRAL, SUBLANG_DEFAULT ),                                              \
                        ( LPTSTR ) caption + count,                                                                \
                        sizeof ( text ) - count,                                                                   \
                        0 );                                                                                       \
        MessageBox ( 0, text, caption, MB_OK | MB_ICONERROR );                                                     \
        __debugbreak ( );                                                                                          \
	}


typedef struct xaudio2_t
{
    IXAudio2               *ixa2;
    IXAudio2SourceVoice    *source;
    IXAudio2MasteringVoice *mastering;
    HANDLE                 semaphore;
	b32                    enabled;
	u32                    length;
    u8                     *buffer;
    u8                     *next_buffer;
} xaudio2_t;


#if 0
typedef struct xaudio2_device_t
{
	u32  device_count;
	i8   devices [ 128 ] [ 256 ]; // No memory allocation, free or shit. I think there is not more devices than 128.
} xaudio2_device_t;


xaudio_device_t xaudio2_get_devices (  )
{
    IXAudio2 *ixa2       = 0;
    u32 device_count     = 0;
    u32 i                = 0;	
	xaudio_devices_t xad = { 0 };	
	CHECK_HRESULT ( XAudio2Create ( &ixa2, 0, XAUDIO2_DEFAULT_PROCESSOR ) );
	CHECK_HRESULT ( IXAudio2_GetDeviceCount ( ixa2, &device_count ) );
	
    for ( i = 0; i < device_count; i++ ) 
	{
        XAUDIO2_DEVICE_DETAILS details = { 0 };
        CHECK_HRESULT ( IXAudio2_GetDeviceDetails ( ixa2, i, &details ) );
		i8 *str = utf16_to_utf8 ( details.DisplayName );
        if ( str ) 
		{
			memcpy ( xad.devices [ i ], str, strlen ( str ) );
        }
    }
    IXAudio2_Release ( ixa2 );
}
#endif

static void STDMETHODCALLTYPE
VoiceCBOnBufferEnd ( IXAudio2VoiceCallback *This, void* data )
{
	xaudio2_t *xa2 = ( xaudio2_t* ) data;
    ReleaseSemaphore ( xa2->semaphore, 1, 0 );
}
static void STDMETHODCALLTYPE
VoiceCBOnVoiceError ( IXAudio2VoiceCallback *This, xaudio2_t *xa2, HRESULT hr )
{
    assert ( 0 );
}
static void STDMETHODCALLTYPE VoiceCBOnStreamEnd             ( IXAudio2VoiceCallback *This ) {}
static void STDMETHODCALLTYPE VoiceCBOnVoiceProcessPassStart ( IXAudio2VoiceCallback *This, UINT32 b ) {}
static void STDMETHODCALLTYPE VoiceCBOnVoiceProcessPassEnd   ( IXAudio2VoiceCallback *This ) {}
static void STDMETHODCALLTYPE VoiceCBOnBufferStart           ( IXAudio2VoiceCallback *This, void *data ) {}
static void STDMETHODCALLTYPE VoiceCBOnLoopEnd               ( IXAudio2VoiceCallback *This, void *data ) {}


static void
xaudio2_play_device ( xaudio2_t *xa2 )
{
    XAUDIO2_BUFFER xaudio2_buffer = { 0 };
    u8 *buffer                    = xa2->buffer;
    u8 *next_buffer               = xa2->next_buffer;
    const u32 length              = xa2->length;
	HRESULT hr                    = S_OK;

    if ( !xa2->enabled ) 
	{ 
        return;
    }

    xaudio2_buffer.AudioBytes = length;
    xaudio2_buffer.pAudioData = next_buffer;
    xaudio2_buffer.pContext   = xa2;

    if ( next_buffer == buffer ) 
	{
        next_buffer += length;
    } 
	else 
	{
        next_buffer = buffer;
    }
	
	xa2->next_buffer = next_buffer;	
	CHECK_HRESULT ( ( hr = IXAudio2SourceVoice_SubmitSourceBuffer ( xa2->source, &xaudio2_buffer, 0 ) ) );
    if ( hr != S_OK)
	{  
        CHECK_HRESULT ( IXAudio2SourceVoice_FlushSourceBuffers ( xa2->source ) );
        xa2->enabled = false;
    }
}

static void
xaudio2_wait_device ( xaudio2_t *xa2 )
{
    if ( xa2->enabled ) 
	{
        WaitForSingleObject ( xa2->semaphore, INFINITE );
    }
}

static void
xaudio2_wait_done ( xaudio2_t *xa2 )
{
    XAUDIO2_VOICE_STATE state   = { 0 };  
    IXAudio2SourceVoice_Discontinuity ( xa2->source );
    IXAudio2SourceVoice_GetState      ( xa2->source, &state, 0  );
    while ( state.BuffersQueued > 0 ) 
	{
        WaitForSingleObject          ( xa2->semaphore, INFINITE );
        IXAudio2SourceVoice_GetState ( xa2->source, &state, 0   );
    }
}


static void xaudio2_close_device ( xaudio2_t *xa2 )
{
        if ( xa2->source ) 
		{
            IXAudio2SourceVoice_Stop ( xa2->source, 0, XAUDIO2_COMMIT_NOW );
            IXAudio2SourceVoice_FlushSourceBuffers ( xa2->source );
            IXAudio2SourceVoice_DestroyVoice       ( xa2->source );
        }
        if ( xa2->ixa2 ) 
		{
            IXAudio2_StopEngine ( xa2->ixa2 );
        }
        if ( xa2->mastering ) 
		{
            IXAudio2MasteringVoice_DestroyVoice ( xa2->mastering );
        }

		SAFE_COM_RELEASE    ( xa2->ixa2 );		
		SAFE_PTR_RELEASE    ( xa2->buffer );
		SAFE_HANDLE_RELEASE ( xa2->semaphore );
		
		CoUninitialize ( );	
}

xaudio2_t xaudio2_open_device ( u32 frequency, u32 channels, u32 bps, b32 is_pcm )
{
    WAVEFORMATEX wfx = { 0 };
    u32 device_id    = 0; 
	
	xaudio2_t xa2 = { 0 };
	
	CHECK_HRESULT ( CoInitializeEx ( 0, COINIT_MULTITHREADED ) );

    static IXAudio2VoiceCallbackVtbl callbacks_vtable = 
	{
        VoiceCBOnVoiceProcessPassStart,
        VoiceCBOnVoiceProcessPassEnd,
        VoiceCBOnStreamEnd,
        VoiceCBOnBufferStart,
        VoiceCBOnBufferEnd,
        VoiceCBOnLoopEnd,
        VoiceCBOnVoiceError
    };

    static IXAudio2VoiceCallback callbacks = { &callbacks_vtable };
    CHECK_HRESULT ( XAudio2Create ( &xa2.ixa2, 0, XAUDIO2_DEFAULT_PROCESSOR ) );
    xa2.semaphore = CreateSemaphore ( 0, 1, 2, 0 );

    xa2.length      = channels * frequency * ( bps / 8 );
    xa2.buffer      = ( u8* ) malloc ( 2 * xa2.length );
    xa2.next_buffer = xa2.buffer;
    memset ( xa2.buffer, 0, ( 2 * xa2.length ) );

    CHECK_HRESULT ( IXAudio2_CreateMasteringVoice ( xa2.ixa2, 
													&xa2.mastering,
													XAUDIO2_DEFAULT_CHANNELS,
												    frequency, 
													0, 
													0,
													0,
													0 ) );
    if ( !is_pcm ) 
	{
        wfx.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
    } 
	else
	{
        wfx.wFormatTag = WAVE_FORMAT_PCM;
    }
	
    wfx.wBitsPerSample  = bps;
    wfx.nChannels       = channels;
    wfx.nSamplesPerSec  = frequency;
    wfx.nBlockAlign     = wfx.nChannels * ( wfx.wBitsPerSample / 8 );
    wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

    CHECK_HRESULT ( IXAudio2_CreateSourceVoice ( xa2.ixa2, 
												 &xa2.source, 
												 &wfx,
												 XAUDIO2_VOICE_NOSRC |
												 XAUDIO2_VOICE_NOPITCH,
												 1.0f, 
												 &callbacks, 
												 0, 
												 0 ) );
    CHECK_HRESULT ( IXAudio2_StartEngine ( xa2.ixa2 ) );
    CHECK_HRESULT ( IXAudio2SourceVoice_Start ( xa2.source, 0, XAUDIO2_COMMIT_NOW ) );
	xa2.enabled = true;
    
	return xa2;
}


#ifdef __cplusplus
}
#endif // !__cplusplus

#endif // !__XAUDIO2_H__